
### 服务配置文件位置

```
/etc/init.d/
```

### 服务管理命令

#### 列出所有可用服务

```
rc-service --list
```

#### 启动/停止/重启 已有服务

```
rc-service {service-name} start/stop/restart
```

#### OR

```
/etc/init.d/{service-name} start/stop/restart
```

### 设置开机自启动

```
rc-update add {service-name}
```
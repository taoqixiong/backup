#!/bin/bash
# save this file to ${HOME}/.config/clash/start-clash.sh

# save pid file
echo $$ > ${HOME}/.config/clash/clash.pid

diff ${HOME}/.config/clash/config.yaml <(curl  -A 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36' -s ${CLASH_URL})
if [ "$?" == 0 ]
then
    /usr/local/bin/clash -d /root/.config/clash/
else
    curl -A 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36' -L -o ${HOME}/.config/clash/config.yaml ${CLASH_URL}
    /usr/local/bin/clash -d /root/.config/clash/
fi